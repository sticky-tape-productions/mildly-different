---
title: "Jamal Sohn"
image: "media/uploads/jamal.jpg"
email: "jamal@example.com"
date: 2021-01-26T10:14:19+06:00
draft: false
social:
  - icon: "la-facebook-f"
    link: "https://facebook.com"
  - icon: "la-twitter"
    link: "https://twitter.com"
  - icon: "la-linkedin-in"
    link: "https://linkedin.com"
---

Jamal Sohn is an Italian journalist, born in 1989, and currently based in San Diego. He graduated from the European Institute of Journalists (IEJ) in 2011.

His stories have appeared in Nature, the Washington Post, NPR, Aeon, bioGraphic and many other publications.
